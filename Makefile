
prefix = /usr
sbindir = $(prefix)/sbin
sysconfdir = /etc
fwconfdir = $(sysconfdir)/firewall
sharedir = $(prefix)/share/firewall
INSTALL = install
TABLES = filter nat mangle raw

all:

clean:

install:
	$(INSTALL) -d $(DESTDIR)$(sbindir)
	$(INSTALL) -d $(DESTDIR)$(sharedir)
	$(INSTALL) -d $(DESTDIR)$(fwconfdir)
	$(INSTALL) -m 755 update-firewall $(DESTDIR)$(sbindir)/update-firewall
	$(INSTALL) -m 755 update-ipset $(DESTDIR)$(sbindir)/update-ipset
	(for t in $(TABLES); do \
	  $(INSTALL) -d $(DESTDIR)$(fwconfdir)/$$t.d ; \
	  $(INSTALL) -d $(DESTDIR)$(sharedir)/$$t.d ; \
	  if [ -d conf-dist/$$t.d ]; then \
	   for f in conf-dist/$$t.d/* ; do \
	    $(INSTALL) -m 644 $$f $(DESTDIR)$(sharedir)/$$t.d ; \
	    b=$$(basename $$f) ; \
	    ln -s $(sharedir)/$$t.d/$$b $(DESTDIR)$(fwconfdir)/$$t.d/$$b ; \
	   done ; \
	  fi ; \
	done)
	(for type in ip net ; do \
	  $(INSTALL) -d $(DESTDIR)$(fwconfdir)/blocked/$$type ; \
	  for proto in ipv4 ipv6 ; do \
	    $(INSTALL) -d $(DESTDIR)$(fwconfdir)/blocked/$$type/$$proto ; \
	  done ; \
	done)
