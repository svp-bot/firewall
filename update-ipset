#!/bin/sh
#
# Create or reload ipset tables using data from /etc/firewall/blocked/
#

basedir=${CONFIG_DIR:-/etc/firewall/blocked}

gen_set() {
    local proto="$1"
    local set_type="$2"
    local set_name="block_${set_type}"
    local family=
    case "${proto}" in
        ipv4)
            family=inet
            ;;
        ipv6)
            family=inet6
            set_name="${set_name}6"
            ;;
    esac

    echo "create ${set_name} hash:${set_type} family ${family} hashsize 1024 maxelem 65536"
    echo "flush ${set_name}"

    local source_dir="${basedir}/${set_type}/${proto}"
    if [ -d "${source_dir}" ]; then
        cat $(run-parts --list "${source_dir}") </dev/null \
            | grep -v '^#' \
            | grep -v '^$' \
            | sed -e "s/^\\(.*\\)\$/add ${set_name} \\1/"
    fi
}

gen_fail2ban() {
    echo "create f2b_ip hash:ip family inet timeout 0"
    echo "create f2b_ip6 hash:ip family inet6 timeout 0"
}

(
    gen_set ipv4 ip
    gen_set ipv6 ip
    gen_set ipv4 net
    gen_set ipv6 net
    gen_fail2ban
) | ipset restore '-!' -exist
